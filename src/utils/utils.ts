// get image base64 code
export function getImage(url: string): HTMLImageElement {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const img_ = require(`@/assets/textures${url}`);
  const img = new Image();
  img.src = img_;
  return img;
}
