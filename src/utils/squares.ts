import * as THREE from "three";
import { Mesh } from "three";
import { getImage } from "./utils";

function createSquare(url: string): Mesh {
  const texture = new THREE.Texture(getImage(url));
  texture.needsUpdate = true;
  const material = new THREE.MeshBasicMaterial({
    map: texture,
  });
  const geometry = new THREE.BoxGeometry(1, 1, 1);
  return new THREE.Mesh(geometry, material);
}

export function createSoil(): Mesh {
  return createSquare("/squares/soil.jpg");
}

export function createBedrock(): Mesh {
  return createSquare("/squares/bedrock.png");
}
