import * as THREE from "three";
import { Camera, Renderer } from "three";
import { PointerLockControls } from "three/examples/jsm/controls/PointerLockControls.js";

export class Control {
  control!: PointerLockControls;
  camera: Camera = new THREE.PerspectiveCamera(
    75,
    window.innerWidth / window.innerHeight,
    0.1,
    30
  );
  constructor(renderer: Renderer) {
    this.camera.position.x = 3;
    this.camera.position.y = 3;
    this.camera.position.z = 3;
    this.control = new PointerLockControls(this.camera, renderer.domElement);
    window.document.onclick = () => {
      this.control.lock();
    };
    window.onkeydown = () => {
      this.camera.position.z += 1;
    };
  }
}
