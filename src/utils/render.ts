import * as THREE from "three";

export class Render {
  scene: THREE.Scene = new THREE.Scene();
  renderer: THREE.WebGLRenderer = new THREE.WebGLRenderer({
    alpha: true,
    antialias: true,
  });
  constructor() {
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.scene.add(new THREE.AxesHelper(100));
    document.body.appendChild(this.renderer.domElement);
  }
}
