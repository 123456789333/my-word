import { Render } from "@/utils/render";
import { Control } from "@/utils/control";
import Stats from "stats-js";

const environment = new Render();

const role = new Control(environment.renderer);

const stats = new Stats();
document.body.appendChild(stats.dom);

function render(): void {
  environment.renderer.render(environment.scene, role.camera);
}

function animate(): void {
  render();
  stats.update();
  requestAnimationFrame(animate);
}

animate();
