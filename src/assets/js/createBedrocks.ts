import { createBedrock } from "@/utils/squares";
import { Scene } from "three";

export function createBedrocks(scene: Scene): void {
  for (let x = 0; x < 10; x++) {
    for (let z = 0; z < 10; z++) {
      const bedrock = createBedrock();
      bedrock.position.x = x + 0.5;
      bedrock.position.y = 0.5;
      bedrock.position.z = z + 0.5;
      scene.add(bedrock);
    }
  }
}
